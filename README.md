# Decentralized Tools for Activists

Tech monopolies gain more and more power. To fight against this dystopia, we
need to organize, without excluding anyone. This works without Google,
Facebook, and Instagram - but what are the alternatives?

Tech-Monopole kontrollieren heute fast die ganze digitale und nicht-digitale
Welt. Um dieser Dystopie etwas entgegenzustellen, müssen wir uns organisieren,
ohne Leute auszuschließen. Das geht auch ohne Google, Facebook, und Instagram -
doch was sind die Alternativen?

## Sources

These websites are great to compare different software solutions, and help you
choosing free alternatives over proprietary.

* https://alternativeto.net/
* https://switching.software/

This very good article is analyzing why GAFAM is a problem:

* https://onezero.medium.com/how-to-destroy-surveillance-capitalism-8135e6744d59

## Reasons for this workshop

Why?

- we want more people to use decentralized tools for political action
- it makes their activism more effective
- decentralized services are more resilient, don't support the monopolies, and are important for privacy
- decentralized services require some extra knowledge
- big tech's monopoly is creating a cyberpunk dystopia

What?

- make people aware of the problem
- teach people how to use the alternatives (effectively)
- it would be great if they can spread the knowledge afterwards

How?

- a workshop on how to use them
- directly trying them out?
- stories on why this is important
- how to find an instance?

## The Problem: Monopolies

Tech monopolies gain more and more power
- their means of production are user data
  - selling advertisements
- knowledge is power:
  - manipulating elections
  - mass surveillance
  - influencing customers
  - repression
  - and we don't know how they can use the data in the future
- most of these services are monopolies; this way they can exploit the data
  - Segmenting: much easier to find people who are interested than with conventional advertising
  - Deception: easier to find people who are easy to deceive, power to withhold information from people
  - Domination: monopoly over all sources of information of a person
  - Mind Control techniques?
    - there are some tricks which manipulate decision making: e.g. auctions, countdowns on purchase websites, "your friends bought this", gameistic addiction tricks
    - Not very effective: most people adapt to techniques like that quickly. Only a fraction of people, those who can't adapt, can be manipulated for longer
- network effects + user lock-in: the more users they have, the more they want

stories?

### Bad Examples:

biggest tech companies: Google, Amazon, Facebook, Apple, Microsoft

eBay, PayPal, Twitter, YouTube (Google), ...

## Decentralized

- But there are alternatives
- Decentralized services
  - not owned by a single company
  - everyone can set up their own server
  - interoperable; the servers can federate with each other
- Also important: Free Software/Open Source
  - everyone is able to use, read, edit, share the source code -> trustworthy

## Sharing Round

- How much do you know about IT, and where did you learn it?
- How much experience do you have with activism and organizing, in any form?
- Which aspects of these topics would you like to learn more about (not necessarily in this workshop)?

## Alternatives

### jitsi

Jitsi Meet is a videoconference-tool, which is easy to use, provides encrypted
communication & which can, but doesn't need to be installed. You can just send
people a link to your channel and they can join.

**Use case:** secure audio/video-chat 

**Website:** https://jitsi.org/

### cryptpad

With CryptPad, you can make quick collaborative documents like spreadsheets,
for taking notes and writing down ideas together. It's a private-by-design
alternative to popular office tools and cloud services. All the content stored
on CryptPad is encrypted before being sent, which means nobody can access your
data unless you give them the link to your pad.

**Use case:** Working collaboratively on press statements, transcripts, or any other text. 

**Website:** https://cryptpad.fr

### wordpress

WordPress is a free and open-source content management system . It was originally created as a blog-publishing system but has evolved to support other types of web content including more traditional mailing lists and forums, media galleries, etc. WordPress can be installed on a web server, like WordPress.com, or a computer running the software package WordPress.org in order to serve as your own network host.

**Use case:** Host your own blog / homepage, where you can easily add posts and pages.

**Website:** https://wordpress.org/ 

**Instances:** blackblogs.org, noblogs.org, ...

### mastodon

Mastodon is a free and open-source self-hosted social network. It allows anyone
to host their own server node in the network, and its various separately
operated user bases are federated across many different servers. These nodes
are referred to as "instances" by Mastodon users. These servers are connected
as a federated social network, allowing users from different servers to
interact with each other seamlessly. The users communicate via short posts
(calles "toots") which can be viewed in ones timeline.

**Use case:** microblogging: quick, short posts to keep the world up-to-date.

**Website:** https://joinmastodon.org/

### mobilizon

A free and federated tool to get events away from Facebook. Create an event
description, date, website. Then people can click ‘participate’ for others to
see. It federates with Mastodon, so people can show that they will attend with
their Mastodon identity.

**Use case:** Invite people who don’t have facebook to an event.

**Website:** https://joinmobilizon.org/

### crab.fit

With crab.fit you can plan an appointment quickly and easily. No registration
is required. You just add some options (like days for a party), send the link
to your friends, and they can click on the the days where they have time.

**Use case:** Find a date for a party, meeting, or action, where everyone has time.

**Website:** https://crab.fit/

### discourse

A classic forum, where people can talk in threads about a topic. Good for
asynchronous communication about a shared cause. Sends mail notifications 

**Use case:** Build a community about a certain topic, to organize a campaign or movement.

**Website:** https://www.discourse.org/

### ownCloud /Nextcloud

ownCloud / Nextcloud is a cloud-hosting software for creating and using file
hosting services. It is free and open-source, thereby allowing anyone to
install and operate it without charge on their own private server. Features
include shareable calendars, audio player etc.

**Use case:** Backup & share your data. Synchronize files across all of your devices.

**Website:** https://owncloud.com/ 

**Instance:** If you need an account, write to servers@schleuder.0x90.space

### kanboard

A special kind of to do list. Kanboard makes it easy to organize tasks, assign
them to people, and track the progress of your project. Great to have an
overview about what’s to be done; but it only really works if the whole team is
dedicated to use it.

**Use case:** Project management; organize a team to get a project done.

**Website:** https://kanboard.org/

**Instance:** If you need an account, write to servers@schleuder.0x90.space

